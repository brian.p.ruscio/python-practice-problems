# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def fizzbuzz(number):
    # If divisible by 3 and 5
    # return "fizzbuzz"
    if number % 3 == 0 and number % 5 == 0:
        return "fizzbuzz"
    # else return fizz if only by 3
    elif number % 3 == 0:
        return "fizz"
    # else buzz if only by
    elif number % 5 == 0:
        return "buzz"
    # else if neither return num
    else:
        return number
    pass

print(fizzbuzz(10))
